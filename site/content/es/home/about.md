---
title: "Pedro Puerta"
image: "_MG_4804.png"
weight: 8
---

Hola, Soy Pedro. Soy misionero, como la yerba, el reviro y nuestra tierra colorada! Estudié afuera, pero en realidad nunca me fuí. Me formé y me preparé como nadie para crear más empleo y mejorar lo que hay, para achicar las diferencias y que sólo existan oportunidades. 

Misiones es única de punta a punta. Con verdes, rojos y celestes que se mezclan en el horizonte, una tierra tan diversa como solidaria. Trabajé siempre en el sector privado, pero vi la política de cerca durante toda mi vida. Tengo ganas de transformar nuestra provincia con ideas nuevas, con cosas que nunca antes se hicieron. 

#Juntos🧉❤ estoy seguro de que podemos hacerlo realidad! 
