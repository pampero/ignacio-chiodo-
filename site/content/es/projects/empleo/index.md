---
title: "Empleo"
weight: 2
---

## Empleo

Trabajar para vivir y no para sobrevivir. 
La esperanza de que el empleo sea un derecho de los misioneros, con más empleo privado y con mejores sueldos en el empleo público. 
Atraer más y mejores inversiones con una Misiones abierta al mundo, aprovechando nuestras ventajas comparativas de frontera