---
title: "Educación"
weight: 1
---
## Educación

Transformar la infraestructura educativa para acercar a las dos Misiones. 

Propiciar la conectividad a partir de planes de inversión privada y pública en las zonas más alejadas de la provincia. 

Quiero generar oportunidades a partir de una mayor oferta educativa, recompensando el esfuerzo del misionero por crecer y superarse.