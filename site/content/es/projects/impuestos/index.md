---
title: "Impuestos"
weight: 2
resources:
    - src: plant.jpg
      params:
          weight: -100
---

## Impuestos

##### QUE VUELVAN 
Es imprescindible que los impuestos vuelvan en mejores servicios públicos. 
Que puedas atenderte en un hospital cerca de tu casa, que te llegue el asfalto, que no tengas que esperar semanas por un trámite.  
Es fundamental alentar la reinversión del dinero público en la sociedad de manera inteligente, con planificación en una misiones del futuro.